﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace PentanaSolutions.Model
{
    public class Store
    {
        private SortedDictionary<string, Fruit> fruits = new SortedDictionary<string, Fruit>(StringComparer.OrdinalIgnoreCase);

        public Store(List<Fruit> fruits)
        {
            fruits.ForEach(x => { if (!this.fruits.ContainsKey(x.Name)) { this.fruits.Add(x.Name, x); } });
        }

        public bool AddFruit(Fruit fruit)
        {
            if (!fruits.ContainsKey(fruit.Name))
            {
                fruits.Add(fruit.Name, fruit);
                return true;
            }

            return false;
        }

        public IEnumerable<Fruit> GetAllFruits()
        {
            return fruits.Values;
        }

        public IEnumerable<Fruit> ReverseAll()
        {
            var reverseFruits = this.fruits.Reverse();
            reverseFruits.ToList().ForEach(x =>
            {
                x.Value.Name = new string(x.Value.Name.Reverse().ToArray());
            });
            return reverseFruits.Select(x => x.Value);
        }

        public Fruit GetFruitByName(string name)
        {
            Fruit fruit;
            fruits.TryGetValue(name, out fruit);
            return fruit ;
        }
    }
}
