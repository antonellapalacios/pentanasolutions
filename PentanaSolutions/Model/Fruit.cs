﻿using System;

namespace PentanaSolutions.Model
{
    public class Fruit : Freshness
    {
        public Fruit(string name, double price, bool in_stock, DateTime expiration_date, DateTime package_date) : base(expiration_date, package_date)
        {
            Name = name;
            Price = price;
            InStock = in_stock;
        }

        public string Name { get; set; }
        public double Price { get; set; }
        public bool InStock { get; set; }
    }
}
