﻿using System;
namespace PentanaSolutions.Model
{
    public class Freshness
    {
        protected DateTime expiration_date;
        protected DateTime package_date;

        public Freshness(DateTime expiration_date, DateTime package_date)
        {
            this.expiration_date = expiration_date;
            this.package_date = package_date;
        }

        public DateTime ExpirationDate { get { return this.expiration_date; } set { this.expiration_date = value; }}
        public DateTime PackageDate { get { return package_date; }}

        public int GetShelfLife()
        {
            return (int)Math.Round((ExpirationDate - PackageDate).TotalDays);
        }

        public bool IsExpired()
        {
            return DateTime.Now > ExpirationDate;
        }

        public int DaysBeforeExpired()
        {
            return (int)Math.Round((ExpirationDate - DateTime.Now).TotalDays);
        }
    }
}
