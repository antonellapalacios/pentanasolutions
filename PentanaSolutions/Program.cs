﻿using System;
using System.Collections.Generic;
using System.Linq;
using PentanaSolutions.Extensions;
using PentanaSolutions.Model;

namespace PentanaSolutions
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Fruit> fruits = new List<Fruit>{
                new Fruit("Berry", 2.3, true, DateTime.Now.AddDays(20), DateTime.Now.AddDays(-5)),
                new Fruit("Apple", 2.5, true, DateTime.Now.AddDays(13), DateTime.Now),
                new Fruit("Banana", 3.5, true, DateTime.Now.AddDays(5), DateTime.Now.AddDays(-3)),
                new Fruit("Kiwi", 1.25, false, DateTime.Now.AddDays(-2), DateTime.Now.AddDays(-12))
            };

            Store store = new Store(fruits);
            bool validOption = false;
            while (!validOption)
            {
                Console.WriteLine("Select option value");
                Console.WriteLine("=================");
                Console.WriteLine("1 - Show current fruits in store");
                Console.WriteLine("2 - Add Fruit");
                Console.WriteLine("3 - Get Freshness Detail for specific fruit");
                Console.WriteLine("4 - Get reverted fruits");
                Console.WriteLine("5 - Exit");

                var option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        ShowCurrentFruits(store);
                        break;
                    case "2":
                        AddFruit(store);
                        break;
                    case "3":
                        GetFreshnessDetails(store);
                        break;
                    case "4":
                        GetRevertedFruits(store);
                        break;
                    case "5":
                        validOption = true;
                        break;
                    default:
                        Console.WriteLine("Invalid Option");
                        break;
                }
            }
        }

        static void ShowList(IEnumerable<Fruit> fruits)
        {
            fruits.ToList().ForEach(x =>
            {
                Console.WriteLine("Name:" + x.Name);
                Console.WriteLine("Price:" + x.Price);
                Console.WriteLine("In Stock:" + x.InStock);
                Console.WriteLine("--------------------");
            });
        }

        static void ShowCurrentFruits(Store store)
        {
            Console.WriteLine("Current Fruits:");
            Console.WriteLine("=================");
            var currentFruits = store.GetAllFruits();
            ShowList(currentFruits);
        }

        static void GetFreshnessDetails(Store store)
        {
            Console.WriteLine("Insert the fruit name:");
            string name = Console.ReadLine();
            Fruit f = store.GetFruitByName(name);
            if (f != null)
            {
                Console.WriteLine(f.Name);
                Console.WriteLine("Is Expired?");
                Console.WriteLine(f.IsExpired().ToYesNoString());
                Console.WriteLine("Expiration Date:");
                Console.WriteLine(f.ExpirationDate.ToShortDateString());
                Console.WriteLine("Package Date:");
                Console.WriteLine(f.PackageDate.ToShortDateString());
                Console.WriteLine("Shelf Life:");
                Console.WriteLine(f.GetShelfLife());
                Console.WriteLine("Days before expired:");
                Console.WriteLine(f.DaysBeforeExpired());
                return;
            }
            Console.WriteLine("The fruit {0} doesn't exist", name);
        }

        static void AddFruit(Store store)
        {
            double price;
            Console.WriteLine("Insert Name");
            string name = Console.ReadLine(); 
            Console.WriteLine("Insert Price without currency");
            double.TryParse(Console.ReadLine(), out price);
            Console.WriteLine("Is it in stock? Insert Y for yes otherwhise it will be No");
            bool instock = Console.ReadLine().ToUpper() == "Y" ? true : false;
            DateTime expirationDate;
            Console.WriteLine("Insert Expiration Date (mm-dd-yyyy)");
            if(!DateTime.TryParse(Console.ReadLine(), out expirationDate))
            {
                Console.WriteLine("Invalid Date");
                return;
            };
            DateTime packageDate;
            Console.WriteLine("Insert Package Date (mm-dd-yyyy)");
            if(!DateTime.TryParse(Console.ReadLine(), out packageDate))
            {
                Console.WriteLine("Invalid Date");
                return;
            };

            if(store.AddFruit(new Fruit(name, price, instock, expirationDate, packageDate)))
            {
                Console.WriteLine("The fruits has been inserted");
                return;
            }

            Console.WriteLine("The fruit already exists, Do you want to add another one? Y for yes");
            if (Console.ReadLine().ToUpper() == "Y")
            {
                AddFruit(store);
            }
        }

        static void GetRevertedFruits(Store store)
        {
            var fruits = store.ReverseAll();
            Console.WriteLine("Reversed fruits");
            ShowList(fruits);
        }
    }
}
