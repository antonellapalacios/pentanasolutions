# My project's README
The class Store contains a collection of fruits, it's implemented by SortedDictionary because the fruits are sorted alphabetically by name.  
I selected the SortedDictionary because for this specific case the exercise implies to add fruits, so I assume that is something important and 
the insertion in SortedDictionary is more efficient than in a SortedList, O(log n) vs O(n), but of course, it always depends if they would be 
a lot of insertions or the action which more repeated is searching specific product. because SortedDictionary uses more memory than SortedList.
Also, I use Linq for all those queries because is simple to read, and simplifies the code.